from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten, Lambda, Cropping2D
from keras.layers.convolutional import Convolution2D
from keras.layers.pooling import MaxPooling2D


import matplotlib.pyplot as plt
import numpy as np




data = np.genfromtxt('./data/driving_log.csv', delimiter=',',dtype=np.unicode_)
im=[]
steer_angle=[]
for i in data:
    # Add central image to array
    im.append(plt.imread(i[0]))
    # Add left image to array
    im.append(plt.imread(i[1]))
    # Add right image to array
    im.append(plt.imread(i[2]))
    # Add central label to array
    steer_angle.append(float(i[3]))
    # Add left label to array with adjustment
    steer_angle.append(float(i[3])+0.3)
    # Add right label to array with adjustment
    steer_angle.append(float(i[3])-0.3)


#Create the Sequential model
model = Sequential()

# chop image
model.add(Cropping2D(cropping=((50,10), (0,0)), input_shape=(160,320,3)))

# Scale data
model.add(Lambda(lambda x: x/255.0 - 0.5, input_shape=(100,320,3)))

# Layer 1
model.add(Convolution2D(64, kernel_size=[7, 7], strides=1, input_shape=(100, 320, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D((2, 2)))

# Layer 2
model.add(Convolution2D(128, kernel_size=[5, 5], strides=1, input_shape=(50, 160, 64)))
model.add(Activation('relu'))
model.add(MaxPooling2D((2, 2)))

# Layer 3
model.add(Convolution2D(256, kernel_size=[5, 5], strides=1, input_shape=(25, 80, 128)))
model.add(Activation('relu'))
model.add(MaxPooling2D((2, 2)))

# Layer 4
model.add(Flatten(input_shape=(25, 80, 256)))
model.add(Dense(1))

# Compile
model.compile(loss='mse',optimizer='adam')
history_object = model.fit(np.array(im),np.array(steer_angle),validation_split=0.2, shuffle=True,epochs=3,verbose=1,batch_size=32)

# Save
model.save('model.h5')




### print the keys contained in the history object
print(history_object.history.keys())

### plot the training and validation loss for each epoch
plt.plot(history_object.history['loss'])
plt.plot(history_object.history['val_loss'])
plt.title('model mean squared error loss')
plt.ylabel('mean squared error loss')
plt.xlabel('epoch')
plt.legend(['training set', 'validation set'], loc='upper right')
plt.show()

